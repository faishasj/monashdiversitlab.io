# diversIT's Website
Static website built with [Hugo](https://gohugo.io/)

# Dev
run `yarn install` then `gulp --watch` to compile the css  
run `hugo server` to run compile the website and serve it

# Deloy
The website will auto-build when pushed to GitLab
