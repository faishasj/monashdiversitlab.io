---
title: "News"
menu: "main"
---
# News
## Newsletter
The [**diversIT newsletter**](https://us15.campaign-archive.com/home/?u=1fe8aa94259585cfc46125cfe&id=a53ae876a4) posts upcoming diversity-related events, job opportunities and general updates once a week. If you're interested, please [**sign up**](http://eepurl.com/cJA5En)!

## Facebook page
Our diversIT team works hard to fill the [**diversIT facebook page**](https://www.facebook.com/monashdiversity/) with relevant articles for you to stay up to date with the latest news in diversity and technology.

The facebook page promotes upcoming meet up events, job opportunities, volunteering opportunities, news articles and more. Please have a look and give it a like if you would like to see more.

<div style="overflow: auto">
  <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmonashdiversity%2F&tabs=timeline&width=500&height=1000&small_header=false&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="500" height="1000" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
</div>
